﻿using MOVIES.Properties;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace MOVIES.DAO
{
   public class DBDao
    {
        protected static SqlConnection GetConnection()
        {
            return new SqlConnection(
                Settings.Default.testConnectionString);
        }
    }
}
