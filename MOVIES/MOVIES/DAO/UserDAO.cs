﻿using MOVIES.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MOVIES.DAO
{
    public class UserDAO : DBDao
    {
        public static List<User> GetUsers()
        {
          List<User> users = new List<User>();
            try
            {
                using (var connection = GetConnection())
                {
                    connection.Open();
                    string expression = @"Select * FROM dbo.Administrator";
                    var command = new SqlCommand(expression, connection);
                    var reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            User user = new User();
                            user.login = reader.GetValue(0).ToString();
                            user.password = reader.GetValue(1).ToString();
                            users.Add(user);
                        }
                    }
                }
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            return users;
        }

        public static Image GetImageFromFile(string inputPath)
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @inputPath);
            Image image = new Bitmap(path);
            return image;
        }
    }
}
