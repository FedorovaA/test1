﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MOVIES.Forms;
using System.Data.SqlClient;
using MOVIES.DAO;

namespace MOVIES.Forms
{
    public partial class SignInForm : BaseForm
    {
        public SignInForm()
        {
            InitializeComponent();
        }

        private void signInButton_Click(object sender, EventArgs e)
        {
            string loginUser = loginTextBox.Text;
            string passwordUser = passwordTextBox.Text;
            var user = UserDAO.GetUsers().
               FirstOrDefault(u => u.login.Equals(loginUser) &&
               u.password.Equals(passwordUser));
            if (user != null)
            {
                LoginController.GetInstance().user = user;
                var menu = new Menu();
                showNextForm(menu);
            }
            else
            {
                MessageBox.Show("Access denied");
            }
        }

        private void noAutorization_Click(object sender, EventArgs e)
        {
            var menu = new Menu();
            showNextForm(menu);
        }
    }
}
