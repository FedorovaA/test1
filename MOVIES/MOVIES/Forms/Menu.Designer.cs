﻿namespace MOVIES.Forms
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            this.moviesBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.moviesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.testDataSet = new MOVIES.testDataSet();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabelYear = new System.Windows.Forms.ToolStripLabel();
            this.toolStripTextBoxYear = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabelGenre = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBoxGenre = new System.Windows.Forms.ToolStripComboBox();
            this.moviesDataGridView = new System.Windows.Forms.DataGridView();
            this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Language = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Runtime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Genre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Director = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Poster = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rated = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.yearDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.languageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.runtimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.genreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.countryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.directorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.posterDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ratedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.moviesTableAdapter = new MOVIES.testDataSetTableAdapters.MoviesTableAdapter();
            this.tableAdapterManager = new MOVIES.testDataSetTableAdapters.TableAdapterManager();
            this.toolStripLabelCountry = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBoxCountry = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabelDirector = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBoxDirector = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabelLanguage = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBoxLanguage = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripLabelRated = new System.Windows.Forms.ToolStripLabel();
            this.toolStripComboBoxRated = new System.Windows.Forms.ToolStripComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.moviesBindingNavigator)).BeginInit();
            this.moviesBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.moviesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moviesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // moviesBindingNavigator
            // 
            this.moviesBindingNavigator.AddNewItem = null;
            this.moviesBindingNavigator.AutoSize = false;
            this.moviesBindingNavigator.BindingSource = this.moviesBindingSource;
            this.moviesBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.moviesBindingNavigator.DeleteItem = null;
            this.moviesBindingNavigator.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.moviesBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.toolStripLabelYear,
            this.toolStripTextBoxYear,
            this.toolStripLabelGenre,
            this.toolStripComboBoxGenre,
            this.toolStripLabelCountry,
            this.toolStripComboBoxCountry,
            this.toolStripLabelDirector,
            this.toolStripComboBoxDirector,
            this.toolStripLabelLanguage,
            this.toolStripComboBoxLanguage,
            this.toolStripLabelRated,
            this.toolStripComboBoxRated});
            this.moviesBindingNavigator.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.moviesBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.moviesBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.moviesBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.moviesBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.moviesBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.moviesBindingNavigator.Name = "moviesBindingNavigator";
            this.moviesBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.moviesBindingNavigator.Size = new System.Drawing.Size(1738, 88);
            this.moviesBindingNavigator.TabIndex = 0;
            this.moviesBindingNavigator.Text = "bindingNavigator1";
            // 
            // moviesBindingSource
            // 
            this.moviesBindingSource.DataMember = "Movies";
            this.moviesBindingSource.DataSource = this.testDataSet;
            this.moviesBindingSource.Sort = "";
            // 
            // testDataSet
            // 
            this.testDataSet.DataSetName = "testDataSet";
            this.testDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.AutoSize = false;
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(75, 34);
            this.bindingNavigatorCountItem.Text = "для {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Общее число элементов";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.AutoSize = false;
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(28, 34);
            this.bindingNavigatorMoveFirstItem.Text = "Переместить в начало";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.AutoSize = false;
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(28, 34);
            this.bindingNavigatorMovePreviousItem.Text = "Переместить назад";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.AutoSize = false;
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 37);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Положение";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 34);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Текущее положение";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.AutoSize = false;
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 37);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.AutoSize = false;
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(28, 34);
            this.bindingNavigatorMoveNextItem.Text = "Переместить вперед";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.AutoSize = false;
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(28, 34);
            this.bindingNavigatorMoveLastItem.Text = "Переместить в конец";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.AutoSize = false;
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 37);
            // 
            // toolStripLabelYear
            // 
            this.toolStripLabelYear.AutoSize = false;
            this.toolStripLabelYear.Name = "toolStripLabelYear";
            this.toolStripLabelYear.Size = new System.Drawing.Size(110, 34);
            this.toolStripLabelYear.Text = "Введите год";
            // 
            // toolStripTextBoxYear
            // 
            this.toolStripTextBoxYear.AutoSize = false;
            this.toolStripTextBoxYear.Margin = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.toolStripTextBoxYear.Name = "toolStripTextBoxYear";
            this.toolStripTextBoxYear.Size = new System.Drawing.Size(140, 34);
            this.toolStripTextBoxYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.toolStripTextBoxYear_KeyPress);
            this.toolStripTextBoxYear.TextChanged += new System.EventHandler(this.toolStripTextBoxYear_TextChanged);
            // 
            // toolStripLabelGenre
            // 
            this.toolStripLabelGenre.AutoSize = false;
            this.toolStripLabelGenre.Name = "toolStripLabelGenre";
            this.toolStripLabelGenre.Size = new System.Drawing.Size(138, 34);
            this.toolStripLabelGenre.Text = "Выберете жанр";
            // 
            // toolStripComboBoxGenre
            // 
            this.toolStripComboBoxGenre.AutoSize = false;
            this.toolStripComboBoxGenre.Items.AddRange(new object[] {
            "Action",
            "Adventure",
            "Animation",
            "Biography",
            "Comedy",
            "Crime",
            "Drama",
            "Family",
            "Fantasy",
            "Film-Noir",
            "History",
            "Horror",
            "Music",
            "Musical",
            "Mystery",
            "Romance",
            "Sci-Fi",
            "Thriller",
            "War",
            "Western"});
            this.toolStripComboBoxGenre.Margin = new System.Windows.Forms.Padding(1);
            this.toolStripComboBoxGenre.Name = "toolStripComboBoxGenre";
            this.toolStripComboBoxGenre.Size = new System.Drawing.Size(124, 34);
            this.toolStripComboBoxGenre.TextChanged += new System.EventHandler(this.toolStripComboBoxGenre_TextChanged);
            // 
            // moviesDataGridView
            // 
            this.moviesDataGridView.AllowUserToAddRows = false;
            this.moviesDataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(209)))), ((int)(((byte)(238)))), ((int)(((byte)(252)))));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.White;
            this.moviesDataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.moviesDataGridView.AutoGenerateColumns = false;
            this.moviesDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.moviesDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.moviesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.moviesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Title,
            this.Year,
            this.Language,
            this.Runtime,
            this.Genre,
            this.Country,
            this.Director,
            this.Poster,
            this.Rated,
            this.titleDataGridViewTextBoxColumn,
            this.yearDataGridViewTextBoxColumn,
            this.languageDataGridViewTextBoxColumn,
            this.runtimeDataGridViewTextBoxColumn,
            this.genreDataGridViewTextBoxColumn,
            this.countryDataGridViewTextBoxColumn,
            this.directorDataGridViewTextBoxColumn,
            this.posterDataGridViewTextBoxColumn,
            this.ratedDataGridViewTextBoxColumn});
            this.moviesDataGridView.DataSource = this.moviesBindingSource;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.moviesDataGridView.DefaultCellStyle = dataGridViewCellStyle18;
            this.moviesDataGridView.Location = new System.Drawing.Point(4, 107);
            this.moviesDataGridView.Name = "moviesDataGridView";
            this.moviesDataGridView.ReadOnly = true;
            this.moviesDataGridView.RowTemplate.Height = 28;
            this.moviesDataGridView.Size = new System.Drawing.Size(1722, 595);
            this.moviesDataGridView.TabIndex = 1;
            // 
            // Title
            // 
            this.Title.DataPropertyName = "Title";
            this.Title.HeaderText = "Title";
            this.Title.Name = "Title";
            this.Title.ReadOnly = true;
            this.Title.Width = 163;
            // 
            // Year
            // 
            this.Year.DataPropertyName = "Year";
            this.Year.HeaderText = "Year";
            this.Year.Name = "Year";
            this.Year.ReadOnly = true;
            this.Year.Width = 162;
            // 
            // Language
            // 
            this.Language.DataPropertyName = "Language";
            this.Language.HeaderText = "Language";
            this.Language.Name = "Language";
            this.Language.ReadOnly = true;
            this.Language.Width = 163;
            // 
            // Runtime
            // 
            this.Runtime.DataPropertyName = "Runtime";
            this.Runtime.HeaderText = "Runtime";
            this.Runtime.Name = "Runtime";
            this.Runtime.ReadOnly = true;
            this.Runtime.Width = 163;
            // 
            // Genre
            // 
            this.Genre.DataPropertyName = "Genre";
            this.Genre.HeaderText = "Genre";
            this.Genre.Name = "Genre";
            this.Genre.ReadOnly = true;
            this.Genre.Width = 162;
            // 
            // Country
            // 
            this.Country.DataPropertyName = "Country";
            this.Country.HeaderText = "Country";
            this.Country.Name = "Country";
            this.Country.ReadOnly = true;
            this.Country.Width = 163;
            // 
            // Director
            // 
            this.Director.DataPropertyName = "Director";
            this.Director.HeaderText = "Director";
            this.Director.Name = "Director";
            this.Director.ReadOnly = true;
            this.Director.Width = 163;
            // 
            // Poster
            // 
            this.Poster.DataPropertyName = "Poster";
            this.Poster.HeaderText = "Poster";
            this.Poster.Name = "Poster";
            this.Poster.ReadOnly = true;
            this.Poster.Width = 162;
            // 
            // Rated
            // 
            this.Rated.DataPropertyName = "Rated";
            this.Rated.HeaderText = "Rated";
            this.Rated.Name = "Rated";
            this.Rated.ReadOnly = true;
            this.Rated.Width = 163;
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.DataPropertyName = "Title";
            this.titleDataGridViewTextBoxColumn.HeaderText = "Title";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            this.titleDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // yearDataGridViewTextBoxColumn
            // 
            this.yearDataGridViewTextBoxColumn.DataPropertyName = "Year";
            this.yearDataGridViewTextBoxColumn.HeaderText = "Year";
            this.yearDataGridViewTextBoxColumn.Name = "yearDataGridViewTextBoxColumn";
            this.yearDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // languageDataGridViewTextBoxColumn
            // 
            this.languageDataGridViewTextBoxColumn.DataPropertyName = "Language";
            this.languageDataGridViewTextBoxColumn.HeaderText = "Language";
            this.languageDataGridViewTextBoxColumn.Name = "languageDataGridViewTextBoxColumn";
            this.languageDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // runtimeDataGridViewTextBoxColumn
            // 
            this.runtimeDataGridViewTextBoxColumn.DataPropertyName = "Runtime";
            this.runtimeDataGridViewTextBoxColumn.HeaderText = "Runtime";
            this.runtimeDataGridViewTextBoxColumn.Name = "runtimeDataGridViewTextBoxColumn";
            this.runtimeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // genreDataGridViewTextBoxColumn
            // 
            this.genreDataGridViewTextBoxColumn.DataPropertyName = "Genre";
            this.genreDataGridViewTextBoxColumn.HeaderText = "Genre";
            this.genreDataGridViewTextBoxColumn.Name = "genreDataGridViewTextBoxColumn";
            this.genreDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // countryDataGridViewTextBoxColumn
            // 
            this.countryDataGridViewTextBoxColumn.DataPropertyName = "Country";
            this.countryDataGridViewTextBoxColumn.HeaderText = "Country";
            this.countryDataGridViewTextBoxColumn.Name = "countryDataGridViewTextBoxColumn";
            this.countryDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // directorDataGridViewTextBoxColumn
            // 
            this.directorDataGridViewTextBoxColumn.DataPropertyName = "Director";
            this.directorDataGridViewTextBoxColumn.HeaderText = "Director";
            this.directorDataGridViewTextBoxColumn.Name = "directorDataGridViewTextBoxColumn";
            this.directorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // posterDataGridViewTextBoxColumn
            // 
            this.posterDataGridViewTextBoxColumn.DataPropertyName = "Poster";
            this.posterDataGridViewTextBoxColumn.HeaderText = "Poster";
            this.posterDataGridViewTextBoxColumn.Name = "posterDataGridViewTextBoxColumn";
            this.posterDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ratedDataGridViewTextBoxColumn
            // 
            this.ratedDataGridViewTextBoxColumn.DataPropertyName = "Rated";
            this.ratedDataGridViewTextBoxColumn.HeaderText = "Rated";
            this.ratedDataGridViewTextBoxColumn.Name = "ratedDataGridViewTextBoxColumn";
            this.ratedDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // moviesTableAdapter
            // 
            this.moviesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.AdministratorTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.CountryTableAdapter = null;
            this.tableAdapterManager.GenreTableAdapter = null;
            this.tableAdapterManager.LanguageTableAdapter = null;
            this.tableAdapterManager.Movie2CountryTableAdapter = null;
            this.tableAdapterManager.Movie2GenreTableAdapter = null;
            this.tableAdapterManager.Movie2LanguageTableAdapter = null;
            this.tableAdapterManager.Movie2ProductionTableAdapter = null;
            this.tableAdapterManager.MovieTableAdapter = null;
            this.tableAdapterManager.Person2MovieTableAdapter = null;
            this.tableAdapterManager.PersonTableAdapter = null;
            this.tableAdapterManager.ProductionTableAdapter = null;
            this.tableAdapterManager.RatedTableAdapter = null;
            this.tableAdapterManager.RoleInMovieTableAdapter = null;
            this.tableAdapterManager.tempTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = MOVIES.testDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // toolStripLabelCountry
            // 
            this.toolStripLabelCountry.AutoSize = false;
            this.toolStripLabelCountry.Name = "toolStripLabelCountry";
            this.toolStripLabelCountry.Size = new System.Drawing.Size(149, 34);
            this.toolStripLabelCountry.Text = "Выберете страну";
            // 
            // toolStripComboBoxCountry
            // 
            this.toolStripComboBoxCountry.AutoSize = false;
            this.toolStripComboBoxCountry.Items.AddRange(new object[] {
            "Brazil",
            "Canada",
            "France",
            "Germany",
            "Hong Kong",
            "Iceland",
            "India",
            "Italy",
            "Japan",
            "New Zealand",
            "Poland",
            "South Korea",
            "Spain",
            "UK",
            "USA",
            "West Germany"});
            this.toolStripComboBoxCountry.Margin = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.toolStripComboBoxCountry.Name = "toolStripComboBoxCountry";
            this.toolStripComboBoxCountry.Size = new System.Drawing.Size(121, 34);
            // 
            // toolStripLabelDirector
            // 
            this.toolStripLabelDirector.AutoSize = false;
            this.toolStripLabelDirector.Name = "toolStripLabelDirector";
            this.toolStripLabelDirector.Size = new System.Drawing.Size(183, 34);
            this.toolStripLabelDirector.Text = "Выберете режиссера";
            // 
            // toolStripComboBoxDirector
            // 
            this.toolStripComboBoxDirector.AutoSize = false;
            this.toolStripComboBoxDirector.Items.AddRange(new object[] {
            "Aamir Khan",
            "Akira Kurosawa",
            "Alfred Hitchcock",
            "Amole Gupte",
            "Andrew Stanton",
            "Billy Wilder",
            "Bryan Singer",
            "Chan-wook Park",
            "Charles Chaplin",
            "Christopher Nolan",
            "Damien Chazelle",
            "Darren Aronofsky",
            "David Fincher",
            "David Lean",
            "Eli Roth",
            "Eric Toledano",
            "Fernando Meirelles",
            "Florian Henckel von Donnersmarck",
            "Francis Ford Coppola",
            "Frank Capra",
            "Frank Darabont",
            "Fritz Lang",
            "Gene Kelly",
            "George Lucas",
            "George Roy Hill",
            "Giuseppe Tornatore",
            "Guy Ritchie",
            "Hayao Miyazaki",
            "Irvin Kershner",
            "Isao Takahata",
            "James Cameron",
            "Jean-Pierre Jeunet",
            "John Lasseter",
            "Jonathan Demme",
            "Katia Lund",
            "Lana Wachowski",
            "Lee Unkrich",
            "Lilly Wachowski",
            "Luc Besson",
            "Makoto Shinkai",
            "Martin Scorsese",
            "Mel Gibson",
            "Michael Curtiz",
            "Michel Gondry",
            "Milos Forman",
            "Nitesh Tiwari",
            "Olivier Nakache",
            "Orson Welles",
            "Peter Jackson",
            "Quentin Tarantino",
            "Rajkumar Hirani",
            "Richard Marquand",
            "Ridley Scott",
            "Rob Minkoff",
            "Robert Mulligan",
            "Robert Zemeckis",
            "Roberto Benigni",
            "Roger Allers",
            "Roman Polanski",
            "Sam Mendes",
            "Sergio Leone",
            "Sidney Lumet",
            "Stanley Donen",
            "Stanley Kubrick",
            "Steven Spielberg",
            "Terry Gilliam",
            "Terry Jones",
            "Tony Kaye",
            "Vittorio De Sica",
            "Wolfgang Petersen"});
            this.toolStripComboBoxDirector.Margin = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.toolStripComboBoxDirector.Name = "toolStripComboBoxDirector";
            this.toolStripComboBoxDirector.Size = new System.Drawing.Size(121, 34);
            // 
            // toolStripLabelLanguage
            // 
            this.toolStripLabelLanguage.AutoSize = false;
            this.toolStripLabelLanguage.Name = "toolStripLabelLanguage";
            this.toolStripLabelLanguage.Size = new System.Drawing.Size(134, 34);
            this.toolStripLabelLanguage.Text = "Выберете язык";
            // 
            // toolStripComboBoxLanguage
            // 
            this.toolStripComboBoxLanguage.AutoSize = false;
            this.toolStripComboBoxLanguage.Items.AddRange(new object[] {
            "Arabic",
            "Cantonese",
            "Chinese",
            "Czech",
            "English",
            "Esperanto",
            "French",
            "German",
            "Hebrew",
            "Hindi",
            "Hungarian",
            "Italian",
            "Japanese",
            "Korean",
            "Latin",
            "Mandarin",
            "Nepali",
            "Old English",
            "Polish",
            "Portuguese",
            "Quenya",
            "Russian",
            "Scottish Gaelic",
            "Sicilian",
            "Sindarin",
            "Spanish",
            "Swahili",
            "Turkish",
            "Vietnamese",
            "Xhosa",
            "Zulu"});
            this.toolStripComboBoxLanguage.Margin = new System.Windows.Forms.Padding(1, 1, 1, 0);
            this.toolStripComboBoxLanguage.Name = "toolStripComboBoxLanguage";
            this.toolStripComboBoxLanguage.Size = new System.Drawing.Size(121, 34);
            // 
            // toolStripLabelRated
            // 
            this.toolStripLabelRated.AutoSize = false;
            this.toolStripLabelRated.Name = "toolStripLabelRated";
            this.toolStripLabelRated.Size = new System.Drawing.Size(159, 25);
            this.toolStripLabelRated.Text = "Выберете рейтинг";
            // 
            // toolStripComboBoxRated
            // 
            this.toolStripComboBoxRated.AutoSize = false;
            this.toolStripComboBoxRated.Items.AddRange(new object[] {
            "APPROVED",
            "G",
            "NOT RATED",
            "PASSED",
            "PG",
            "PG-13",
            "R",
            "UNRATED"});
            this.toolStripComboBoxRated.Margin = new System.Windows.Forms.Padding(2, 1, 2, 2);
            this.toolStripComboBoxRated.Name = "toolStripComboBoxRated";
            this.toolStripComboBoxRated.Size = new System.Drawing.Size(121, 33);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(213)))), ((int)(((byte)(202)))));
            this.ClientSize = new System.Drawing.Size(1738, 1050);
            this.Controls.Add(this.moviesDataGridView);
            this.Controls.Add(this.moviesBindingNavigator);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1760, 1760);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1760, 1078);
            this.Name = "Menu";
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.Menu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.moviesBindingNavigator)).EndInit();
            this.moviesBindingNavigator.ResumeLayout(false);
            this.moviesBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.moviesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moviesDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private testDataSet testDataSet;
        private System.Windows.Forms.BindingSource moviesBindingSource;
        private testDataSetTableAdapters.MoviesTableAdapter moviesTableAdapter;
        private testDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator moviesBindingNavigator;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.DataGridView moviesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn Year;
        private System.Windows.Forms.DataGridViewTextBoxColumn Language;
        private System.Windows.Forms.DataGridViewTextBoxColumn Runtime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Genre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Country;
        private System.Windows.Forms.DataGridViewTextBoxColumn Director;
        private System.Windows.Forms.DataGridViewTextBoxColumn Poster;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rated;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn yearDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn languageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn runtimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn genreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn countryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn directorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn posterDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ratedDataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStripLabel toolStripLabelYear;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxYear;
        private System.Windows.Forms.ToolStripLabel toolStripLabelGenre;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxGenre;
        private System.Windows.Forms.ToolStripLabel toolStripLabelCountry;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxCountry;
        private System.Windows.Forms.ToolStripLabel toolStripLabelDirector;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxDirector;
        private System.Windows.Forms.ToolStripLabel toolStripLabelLanguage;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxLanguage;
        private System.Windows.Forms.ToolStripLabel toolStripLabelRated;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBoxRated;
    }
}