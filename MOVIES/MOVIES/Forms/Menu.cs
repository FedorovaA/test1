﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MOVIES.Forms
{
    public partial class Menu : BaseForm
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void Menu_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "testDataSet.Movies". При необходимости она может быть перемещена или удалена.
            this.moviesTableAdapter.Fill(this.testDataSet.Movies);
            this.moviesBindingSource.Sort = "Rated DESC";
            foreach (DataGridViewRow row in moviesDataGridView.Rows)

                if (row.Index % 2 == 0)
                {
                    row.DefaultCellStyle.BackColor = Color.FromArgb(255,252,214);
                }
            toolStripTextBoxYear.MaxLength = 4;
        }

        private void toolStripTextBoxYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if(!Char.IsDigit(number) && number!=8)
            {
                e.Handled = true;
            }
        }

        private void toolStripTextBoxYear_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(toolStripTextBoxYear.Text))
            {
                moviesBindingSource.Filter = "Convert ([Year],'System.String') LIKE '%" + toolStripTextBoxYear + "%'";
            }
            else
            {
                moviesBindingSource.Filter = "";
            }
        }

        private void toolStripComboBoxGenre_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(toolStripComboBoxGenre.Text))
            {
                moviesBindingSource.Filter = "Convert ([Genre],'System.String') LIKE '%" + toolStripComboBoxGenre.Text + "%'";
            }
            else
            {
                moviesBindingSource.Filter = "";
            }
        }
    }
}
